<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $autor
 * @property string $listado
 * @property string $fecha
 * @property string $foto
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor'], 'required'],
            [['fecha'], 'safe'],
            [['autor'], 'string', 'max' => 50],
            [['listado'], 'string', 'max' => 300],
            [['foto'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'autor' => 'Autor',
            'listado' => 'Listado',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
        ];
    }
}
