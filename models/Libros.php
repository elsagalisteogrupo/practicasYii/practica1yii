<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string $titulo
 * @property string $mensaje
 * @property string $fecha
 * @property string $foto
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
            [['mensaje'], 'string', 'max' => 300],
            [['foto'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'mensaje' => 'Mensaje',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
        ];
    }
}
