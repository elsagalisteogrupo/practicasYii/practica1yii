<?php
    use yii\helpers\Html;
?>

    <div>Autor: <?= $dato->autor ?></div>
    <div>Listado: <?= $dato->listado ?></div>
    <div>Fecha: <?= $dato->fecha ?></div>
    <div>Foto: <?= $dato->foto ?></div>
    <p><?= Html::img('@web/imgs/' . $dato->foto, ['alt' => 'Foto 1']) ?></p>

