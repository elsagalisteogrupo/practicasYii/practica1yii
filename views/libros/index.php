<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'Página {page} de {pageCount}',
        'columns' => [
            

            'id',
            'titulo',
            'mensaje',
            'fecha',
            
            [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($valor){
                return Html::img('@web/imgs/' . $valor->foto, ['alt' => 'Foto']);
            }
            ],
            
	],
    ]); ?>
</div>


